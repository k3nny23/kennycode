import {NgModule, } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AboutUsComponent} from './about-us.component';

@NgModule({
  declarations: [AboutUsComponent],
  imports: [
    BrowserModule
  ],
  providers: [],
})
export class AboutUsModule {
}
