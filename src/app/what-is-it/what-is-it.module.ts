import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {WhatIsItComponent} from './what-is-it.component';

@NgModule({
  declarations: [WhatIsItComponent],
  imports: [BrowserModule],
  providers: [],
})
export class WhatIsItModule {
}
