import {NgModule, } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {ProjectListComponent} from './project-list.component';

@NgModule({
  declarations: [ProjectListComponent],
  imports: [
    BrowserModule
  ],
  providers: [],
})
export class ProjectListModule {
}
