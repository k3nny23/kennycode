import {Component, OnInit} from '@angular/core';
import {Project} from '../project';

import {ProjectService} from '../_service/project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
})
export class ProjectListComponent implements OnInit {
  projects: Project[] = [];

  order = 'asc';

  orderBy(value: string): void {
    if (this.order !== value) {
      this.order = value;
      this.projects = this._projectService.orderByField('name', this.order);
    }
  }

  filterBy(value: string): void {
    if (value !== '') {
      this.projects = this._projectService.filterByField('name', value);
    } else {
      this.projects = this._projectService.getAll();
    }
  }

  constructor(private _projectService: ProjectService) {}

  ngOnInit() {
    this.projects = this._projectService.getAll();
  }
}
