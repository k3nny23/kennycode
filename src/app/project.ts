export interface Project {
  id: number;
  name: string;
  description: string;
  cover: string;
  categories: string[];
  path: string;
  date: string;
  version: string;
  author: string;
}
