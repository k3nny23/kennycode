import {Injectable} from '@angular/core';

@Injectable()
export class ArrayHelper {
  constructor() {}

  filter(array: any, value: string): any {
    console.log(array);
    return array.filter((item) =>
      item.name.toLowerCase() === value.toLowerCase())
  }
}
