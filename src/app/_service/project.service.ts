import {Injectable} from '@angular/core';
import {Project} from '../project';



@Injectable()
export class ProjectService {
  constructor() {
  }

  projects: Project[] = [
    {
      id: 1,
      name: 'A',
      description: 'Text messaging, or texting, is the act of composing and sending electronic messages, typically consisting of...',
      cover: '/assets/img/a1.jpg',
      version: '7.21c',
      date: '21-1-2888',
      path: '',
      categories: ['SpringMVC', 'CSS', 'Jquary', 'Angular', 'Java', 'MySQL'],
      author: 'kenny'
    },
    {
      id: 2,
      name: 'B',
      description: 'Unlimited talk, text & data. Our phones come with a 30-day money-back guarantee and 1 year warranty. If you ...',
      cover: '/assets/img/b1.jpg',
      version: '12.67',
      date: '21-1-2888',
      path: '',
      categories: ['E1', 'E2', 'E3'],
      author: 'snake'
    }
    ,
    {
      id: 3,
      name: 'C',
      description: 'Unlimited talk, text & data. Our phones come with a 30-day money-back guarantee and 1 year warranty. If you ...',
      cover: '/assets/img/b1.jpg',
      version: '0.75b',
      date: '21-1-2888',
      path: '',
      categories: ['E1', 'E2', 'E3', 'ZE3'],
      author: 'milkwalk'
    }
    ,
    {
      id: 4,
      name: 'D',
      description: 'Unlimited talk, text & data. Our phones come with a 30-day money-back guarantee and 1 year warranty. If you ...',
      cover: '/assets/img/b1.jpg',
      version: '3.1z',
      date: '21-1-2888',
      path: '',
      categories: ['E1', 'E2', 'E3'],
      author: 'theguy'
    }
  ]
  projectsFiltered: Project[];
  projectsSorted: Project[];


  filterByField(field: string, value: string): Project[] {
    // console.log('filter by field: ' + field + ' | value:' + value);
    this.projectsFiltered = this.projects;

    return this.projectsFiltered.filter(project => project.name.toLowerCase().includes(value.toLowerCase()));
  }

  /**
   * Order the projects only array
   */
  orderByField(field: string, value: string): Project[] {
    // console.log('order by field: ' + field + ' | value:' + value);
    this.projectsSorted = this.projects;
    if (value === 'asc') {
      return this.projectsSorted.sort(function(a, b) {
        if (a.name.toLowerCase() < b.name.toLowerCase()) {
          return -1;
        } else if (a.name.toLowerCase() > b.name.toLowerCase()) {
          return 1;
        }
        return 0;

      });
    } else {
      return this.projectsSorted.reverse();
    }

  }

  getAll(): Project[] {
    return this.projects;
  }

}
