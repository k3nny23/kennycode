import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {WhatIsItComponent} from './what-is-it/what-is-it.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ProjectListComponent} from './project-list/project-list.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

// Router config let's you map routes to compoents
const routes: Routes = [
  {path: 'projects', component: ProjectListComponent},
  {path: 'whatisit', component: WhatIsItComponent},
  {path: 'aboutus', component: AboutUsComponent},
  {
    path: '',
    redirectTo: 'projects',
    pathMatch: 'full'
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
