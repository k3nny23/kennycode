import {Component, AfterViewInit, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Kenny Code';

  constructor(private router: Router) {}

  whereAmI(): string {
    return this.router.url;
  }
  
  show() {
    $('.ui.modal').modal('show');
  }
  ngAfterViewInit() {
    console.log('afterViewInit haha');
  }

  ngOnInit() {
    this.whereAmI();
  }
}
