import {Component, Input, OnInit, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-modal',
  template: `
  <div class="ui modal">
    <div class="ui modal">
        <div class="header">
          <ng-content select=".app-modal-header"></ng-content>
        </div>
        <div class="contant">
          <ng-content select=".app-modal-contant"></ng-content>
        </div>
        <div class="actions">
          <ng-content select=".app-modal-actions"></ng-content>
        </div>
    </div>
  </div>
  `
})
export class ModalComponent {


  public open(): void {

  }

  public close(): void {

  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.close();
    }
  }
}
