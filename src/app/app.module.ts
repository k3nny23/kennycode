import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ProjectListModule} from './project-list/project-list.module';
import {AboutUsModule} from './about-us/about-us.module';
import {WhatIsItModule} from './what-is-it/what-is-it.module';

import {ProjectService} from './_service/project.service';

import {ModalComponent} from './_directive/modal.component';

import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ProjectListModule,
    AboutUsModule,
    WhatIsItModule
  ],
  providers: [ProjectService],
  bootstrap: [AppComponent]
})
export class AppModule {}
